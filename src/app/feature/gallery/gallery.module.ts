import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { GalleryRoutingModule } from './gallery-routing.module';
import { OurCultureComponent } from './our-culture/our-culture.component';


@NgModule({
  declarations: [OurCultureComponent],
  imports: [
    CommonModule,
    GalleryRoutingModule,
    SharedModule
  ]
})
export class GalleryModule { }
