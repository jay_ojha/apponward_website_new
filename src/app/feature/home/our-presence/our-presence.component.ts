import {
  Component,
  OnInit,
  AfterViewInit,
  Injectable,
  Inject,
  HostListener,
  PLATFORM_ID,
} from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-our-presence',
  templateUrl: './our-presence.component.html',
  styleUrls: ['./our-presence.component.css']
})
export class OurPresenceComponent implements OnInit {

  constructor( @Inject(PLATFORM_ID)
  private platformId: any) { }

  ngOnInit(): void {
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    // debugger;
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
    sessionStorage.setItem('back_btn', 'true');
    }
  }
}
