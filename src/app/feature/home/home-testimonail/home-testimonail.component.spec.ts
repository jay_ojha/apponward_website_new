import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeTestimonailComponent } from './home-testimonail.component';

describe('HomeTestimonailComponent', () => {
  let component: HomeTestimonailComponent;
  let fixture: ComponentFixture<HomeTestimonailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeTestimonailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeTestimonailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
