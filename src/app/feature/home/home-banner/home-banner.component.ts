import {
  Component,
  OnInit,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-home-banner',
  templateUrl: './home-banner.component.html',
  styleUrls: ['./home-banner.component.css'],
})
export class HomeBannerComponent implements OnInit {
  constructor(
    @Inject(PLATFORM_ID)
    private platformId: any
  ) {}
  ngOnInit(): void {}
  ngAfterViewInit() {
    this.bannerSlider();
  }
  typedTextSpan: any;
  textArray: any;
  cursorSpan: any;
  bannerSlider() {
    this.textArray = [];
    this.textArray = [
      'iPhone App Development',
      'Android App Development',
      'iPad App Development',
      'Website Development',
      'UI/UX Designs',
      'Cloud Computing Services',
      'Digital Marketing',
      'Search Engine Optimization',
      'App Store Optimization',
    ];
    this.type();
  }

  charIndex = 0;
  textArrayIndex = 0;
  typingDelay = 200;
  erasingDelay = 100;
  newTextDelay = 2000; // Delay between current and next text

  type() {
    try{
    this.typedTextSpan = document.querySelector('.typed-text');
    this.cursorSpan = document.querySelector('.cursor');

    if (this.charIndex < this.textArray[this.textArrayIndex].length) {
      if (!this.cursorSpan.classList.contains('typing'))
        this.cursorSpan.classList.add('typing');
      this.typedTextSpan.textContent += this.textArray[
        this.textArrayIndex
      ].charAt(this.charIndex);
      this.charIndex++;
      setTimeout(() => {
        this.type();
      }, this.typingDelay);
    } else {
      this.cursorSpan.classList.remove('typing');
      setTimeout(() => {
        this.erase();
      }, this.newTextDelay);
    }
  }
  catch(e){}
  }
  erase() {
    if (this.charIndex > 0) {
      if (!this.cursorSpan.classList.contains('typing'))
        this.cursorSpan.classList.add('typing');
      this.typedTextSpan.textContent = this.textArray[
        this.textArrayIndex
      ].substring(0, 0);
      this.charIndex--;
      setTimeout(() => {
        this.erase();
      }, 0);
    } else {
      this.cursorSpan.classList.remove('typing');
      this.textArrayIndex++;
      if (this.textArrayIndex >= this.textArray.length) this.textArrayIndex = 0;
      setTimeout(() => {
        this.type();
      }, 0);
    }
  }
}
