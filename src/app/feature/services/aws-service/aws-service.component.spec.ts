import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AwsServiceComponent } from './aws-service.component';

describe('AwsServiceComponent', () => {
  let component: AwsServiceComponent;
  let fixture: ComponentFixture<AwsServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AwsServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AwsServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
