import {
  Component,
  OnInit,
  AfterViewInit,
  Injectable,
  Inject,
  HostListener,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-maetrix',
  templateUrl: './maetrix.component.html',
  styleUrls: ['./maetrix.component.css']
})
export class MaetrixComponent implements OnInit {
  title = 'The Maetrix | Best Learning platform | Apponward ';
  constructor(
    private titleService: Title,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private metaService: Meta,) { 

    }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {name: 'description', content: 'The Maetrix is an LMS whose prime users are Aesthetic Practitioners. It is one of the Best Marketing Consultant Websites in the Apponward portfolio.'},
      {name: 'og:image', content: 'https://nzjhgtms.apponward.com/assets/images/apponward.gif'},
      {name: 'keywords', content: 'The Maetrix Portfolio, The Maetrix, Best Marketing Consultant Website, Best Website Development Company,  Apponward portfolio, Top App Development Company, Apponward Technologies'},
      {name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.'},
      {name: 'author', content: 'Apponward Technologies Pvt. Ltd.'},
      {name: 'robots', content: 'index, follow'},
      { name: 'twitter:card', content: 'The Maetrix is an LMS whose prime users are Aesthetic Practitioners. It is one of the Best Marketing Consultant Websites in the Apponward portfolio.'},
      { name: 'twitter:site', content:''},
      { name: 'twitter:creator', content:''}
    ]);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    // debugger;
    // console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
    sessionStorage.setItem('back_btn', 'true');
    }
  }
}
