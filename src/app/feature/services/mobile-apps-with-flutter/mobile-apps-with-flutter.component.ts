import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-mobile-apps-with-flutter',
  templateUrl: './mobile-apps-with-flutter.component.html',
  styleUrls: ['./mobile-apps-with-flutter.component.css'],
})
export class MobileAppsWithFlutterComponent implements OnInit {
  title = 'Best Flutter App Development Company | Apponward';
  constructor(
    private titleService: Title,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private metaService: Meta
  ) {}
  navigationFn() {
    if (isPlatformBrowser(this.platformId)) {
      let back_history = sessionStorage.getItem('back_landing_section');
      if (back_history == 'blogHome') {
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('blogHome')
            ? document.getElementById('blogHome')
            : '0';
          let h = testDiv.offsetTop;
          window.scrollTo(h - 600, h);
        }, 0); //5s
      }
    }
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'Apponward is a leading provider of Flutter app development services. Our skilled developers will help you with mobile app development with flutter and dart.',
      },
      {
        name: 'og:image',
        content: 'https://nzjhgtms.apponward.com/assets/images/apponward.gif',
      },
      {
        name: 'keywords',
        content:
          'how to develop android app using flutter mobile app development with flutter mobile app development using flutter how to build an app using flutter mobile app development with flutter and dart how to create an app using flutter  mobile app development tools flutter mobile app development in flutter Flutter app development services',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'Apponward is a leading provider of Flutter app development services. Our skilled developers will help you with mobile app development with flutter and dart.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
    setTimeout(() => {
      this.navigationFn();
    }, 5000);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_landing_section', 'services');
    }
  }
}
