import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PwaWithAngularReactComponent } from './pwa-with-angular-react.component';

describe('PwaWithAngularReactComponent', () => {
  let component: PwaWithAngularReactComponent;
  let fixture: ComponentFixture<PwaWithAngularReactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PwaWithAngularReactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PwaWithAngularReactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
