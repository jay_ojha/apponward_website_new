import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaetrixComponent } from './maetrix.component';

describe('MaetrixComponent', () => {
  let component: MaetrixComponent;
  let fixture: ComponentFixture<MaetrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaetrixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaetrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
