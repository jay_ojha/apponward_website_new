import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebAppDevelopmentWithPythonDjangoComponent } from './web-app-development-with-python-django.component';

describe('WebAppDevelopmentWithPythonDjangoComponent', () => {
  let component: WebAppDevelopmentWithPythonDjangoComponent;
  let fixture: ComponentFixture<WebAppDevelopmentWithPythonDjangoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebAppDevelopmentWithPythonDjangoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebAppDevelopmentWithPythonDjangoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
