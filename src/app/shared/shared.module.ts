import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedRoutingModule } from './shared-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ToastrModule } from 'ngx-toastr';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularButtonLoaderModule } from 'angular-button-loader';
import { HttpClientModule } from '@angular/common/http';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { LazyLoadImageModule } from 'ng-lazyload-image'; 
@NgModule({
  declarations: [HeaderComponent, FooterComponent],
  imports: [
    CommonModule,
    SharedRoutingModule,
    ToastrModule.forRoot({timeOut: 5000}),
    ReactiveFormsModule,
    FormsModule,
    AngularButtonLoaderModule.forRoot(),
    HttpClientModule,
    NgxCaptchaModule,
    LazyLoadImageModule,
    NgxIntlTelInputModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent
    
  ]
})
export class SharedModule { }
