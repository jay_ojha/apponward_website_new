import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { from } from 'rxjs';
const axios = require('axios');
import { environment } from '../../../../environments/environment';
import { ApponwardService } from 'src/app/_services/apponward.services';

@Component({
  selector: 'app-career',
  templateUrl: './career.component.html',
  styleUrls: ['./career.component.css'],
})
export class CareerComponent implements OnInit {
  title = 'Job Openings | Careers at Apponward ';
  careerForm: any = FormGroup;
  isFormSubmitted: boolean = false;
  imageFile: any;
  loading: any;
  careerData: any = [];
  applyNow = 'Apply Now';
  submitTxt = 'Submit';
  showMe: any;
  msg = '';
  showmessage = true;

  constructor(
    private titleService: Title,
    private metaService: Meta,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private toastr: ToastrService,
    private apponwardService: ApponwardService,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private toastrService: ToastrService
  ) {
    this.getCareerData();
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'Come and give a flight to your career with Apponward. Apply at multiple Job Openings in Apponward. Apply on multiple open positions at Apponward.',
      },
      {
        name: 'og:image',
        content: 'https://nzjhgtms.apponward.com/assets/images/apponward.gif',
      },
      {
        name: 'keywords',
        content:
          'Jobs At Apponward, Career with Apponward, Careers, Jobs, Careers in Apponward, Developer Jobs, App development Job Openings',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'Come and give a flight to your career with Apponward. Apply at multiple Job Openings in Apponward. Apply on multiple open positions at Apponward.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
    this.careerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      position: ['', [Validators.required]],
      number: ['', [Validators.required]],
      message: ['', [Validators.required]],
      file: ['', [Validators.required]],
    });
  }
  get f() {
    //console.log(this.addSubscriberForm.controls);
    return this.careerForm.controls;
  }
  onCareerForm() {
    this.isFormSubmitted = true;
    if (this.careerForm.invalid) {
      return;
    }
    this.loading = true;
    this.applyNow = 'Sending';
    console.log('form values=====', JSON.stringify(this.careerForm.value));

    //return false;
    // let reqData = {
    //SubscriberId: 10,
    //   name: this.careerForm.value.name,
    //   email: this.careerForm.value.email,
    //   mobile: this.careerForm.value.mobile,
    //   position: this.careerForm.value.position,
    //   message: this.careerForm.value.message,
    // };
    //let reqDatas = JSON.stringify(reqData);
    // console.log('reqdata===', reqData);
    //return false;
    var formData: any = new FormData();
    formData.append('name', this.careerForm.value.name);
    formData.append('email', this.careerForm.value.email);
    formData.append('number', this.careerForm.value.number);
    formData.append('message', this.careerForm.value.message);
    formData.append('position', this.careerForm.value.position);
    formData.append('file', this.imageFile);

    // let url = `${environment.apiUrl}/jobApply`;
    let url = `https://admin.apponward.com/api/career`;

    let config = {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      responseType: 'blob',
    };
    this.showMe = setTimeout(() => {
      this.showmessage = true;
      this.msg =
        'Thanks for your interest with us!\n Apponward team will get back to you soon.';
      this.loading = true;
      this.submitTxt = 'Sending';
      this.careerForm.disable();
    }, 0);
    this.showMe = setTimeout(() => {
      this.showmessage = false;
      this.msg =
        'Thanks for your interest with us!\n Apponward team will get back to you soon.';
      this.loading = false;
      this.submitTxt = 'Apply Now';
      this.careerForm.reset();
      this.careerForm.disable();
    }, 3000);

    this.http.post(url, formData).subscribe(
      (data: any) => {
        console.log('adata===============', url);
        console.log('data is here', data);
        // this.toastr.success("Thanks for your interest with us!\n Apponword team will get back to you soon.");
        // this.router.navigate(['/careers']);
        // location.reload();
        this.showMe = setTimeout(() => {
          this.showmessage = true;
          this.msg =
            'Thanks for your interest with us!\n Apponward team will get back to you soon.';
          this.submitTxt = 'Apply Now';
        }, 10000);
        let element = document.getElementById('exampleModal') as HTMLElement;
        element.click();
        this.isFormSubmitted = false;
        this.loading = false;
        this.careerForm.reset();
        this.careerForm.enable();
        this.submitTxt = 'Apply Now';
        this.applyNow = 'Apply Now';
      },
      (error) => {
        console.log('error', error);
        //this.buttonLoaderService.hideLoader();
        this.toastr.error('Some error occured! please try again.');
      }
    );
  }
  onSelectFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.imageFile = event.target.files[0];
      console.log('image file ====', this.imageFile);
    }
  }

  getCareerData() {
    this.apponwardService.getCareerList().subscribe(
      (data: any) => {
        this.careerData = data.result;
        console.log(this.careerData);
      },
      (error) => {
        this.toastrService.error(error.error.message || 'Unknown error');
      }
    );
  }
  viewDetails(title_url: any) {
    this.router.navigate(['/careers', title_url]);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    // debugger;
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_btn', 'true');
    }
  }
}
