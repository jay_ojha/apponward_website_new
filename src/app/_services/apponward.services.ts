import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "../../environments/environment";
@Injectable({
  providedIn: 'root'
})
export class ApponwardService {
  baseUrl1 = environment.apiUrl1;
  constructor(
    private http: HttpClient
  ) { }
  getTeam(){
    return this.http.get(this.baseUrl1 + `teamList`); 
  }
  getTxtTestimonail(){
    return this.http.get(this.baseUrl1 + `testimonialListImage`);
  }
  getVdoTestimonail(){
    return this.http.get(this.baseUrl1 + `testimonialListVideo`);
  }
  getCareerList(){
    return this.http.get(this.baseUrl1 + `jobList`);
  }
  getBlogList(){
    return this.http.get(this.baseUrl1 + `blogList`);
  }
  getBlogDetail(reqData:any){
    return this.http.get(this.baseUrl1 + `blogDetail?title=${reqData}`);
  }
  getBlogComment(reqData:any){
    return this.http.get(this.baseUrl1 + `commentsList?blog_id=${reqData}`);
  }
  getJobDetail(reqData:any){
    return this.http.get(this.baseUrl1 + `careerDetail?title=${reqData}`);
  }  
  getHomeTxtTestimonail(){
    return this.http.get(this.baseUrl1 + `homePageItemsText`);
  }
  getValidateUrl(reqData:any){
    const headers = { 'Content-Type': 'application/json'}  
    return this.http.post(this.baseUrl1 + `validateUrl`,reqData,{'headers':headers});
  }
}
