import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogsComponent } from './blogs/blogs.component'
import { BlogDetailsComponent } from './blog-details/blog-details.component'
import { BlogDescriptionComponent } from './blog-description/blog-description.component';

const routes: Routes = [
  { path: 'blogs', component: BlogsComponent },
  { path: 'blogs/:title_url', component: BlogDetailsComponent },
  { path: 'blogs/:title_url', component: BlogDescriptionComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
