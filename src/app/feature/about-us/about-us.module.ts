import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module'
import { AboutUsRoutingModule } from './about-us-routing.module';
import { AboutUsComponent } from './about-us/about-us.component';
import { AboutBannerComponent } from './about-banner/about-banner.component';
import { AboutWorkComponent } from './about-work/about-work.component';
import { OurProcessComponent } from './our-process/our-process.component';
import { OurStatsComponent } from './our-stats/our-stats.component';
import { OurCultureComponent } from './our-culture/our-culture.component';
import { OurFactsDataComponent } from './our-facts-data/our-facts-data.component';
import { ToastrModule } from 'ngx-toastr';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularButtonLoaderModule } from 'angular-button-loader';
import { HttpClientModule } from '@angular/common/http';

import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';

@NgModule({
  declarations: [AboutUsComponent, AboutBannerComponent, AboutWorkComponent, OurProcessComponent, OurStatsComponent, OurCultureComponent, OurFactsDataComponent],
  imports: [
    CommonModule,
    AboutUsRoutingModule,
    SharedModule,
    ToastrModule.forRoot({timeOut: 5000}),
    ReactiveFormsModule,
    FormsModule,
    AngularButtonLoaderModule.forRoot(),
    HttpClientModule,
    NgxIntlTelInputModule
  ]
})
export class AboutUsModule { }
