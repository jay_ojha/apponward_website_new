import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ServicesRoutingModule } from './services-routing.module';
import { ServicesComponent } from './services/services.component';
import { AndroidApplicationServiceComponent } from './android-application-service/android-application-service.component';
import { IosApplicationServiceComponent } from './ios-application-service/ios-application-service.component';
import { WebApplicationServiceComponent } from './web-application-service/web-application-service.component';
import { UiUxServiceComponent } from './ui-ux-service/ui-ux-service.component';
import { HybridMobileAppServiceComponent } from './hybrid-mobile-app-service/hybrid-mobile-app-service.component';
import { AwsServiceComponent } from './aws-service/aws-service.component';
import { DigitalMarketingServiceComponent } from './digital-marketing-service/digital-marketing-service.component';
import { SocialMediaMarketingComponent } from './social-media-marketing/social-media-marketing.component';
import { SearchEngineOptimizationComponent } from './search-engine-optimization/search-engine-optimization.component';
import { SearchEngineMarketingComponent } from './search-engine-marketing/search-engine-marketing.component';
import { ContentWritingComponent } from './content-writing/content-writing.component';
import { MobileAppDevelopmentComponent } from './mobile-app-development/mobile-app-development.component';
import { MobileAppsWithFlutterComponent } from './mobile-apps-with-flutter/mobile-apps-with-flutter.component';
import { MobileAppDevelopmentWithReactComponent } from './mobile-app-development-with-react/mobile-app-development-with-react.component';
import { LogoDesignComponent } from './logo-design/logo-design.component';
import { UiUxForWebAppsComponent } from './ui-ux-for-web-apps/ui-ux-for-web-apps.component';
import { UiUxForMobileAppsComponent } from './ui-ux-for-mobile-apps/ui-ux-for-mobile-apps.component';
import { PrototypingComponent } from './prototyping/prototyping.component';
import { WebAppDevelopmentWithPhpLaravelComponent } from './web-app-development-with-php-laravel/web-app-development-with-php-laravel.component';
import { WebAppDevelopmentWithPythonDjangoComponent } from './web-app-development-with-python-django/web-app-development-with-python-django.component';
import { WebAppDevelopmentWithAngularAndNodeComponent } from './web-app-development-with-angular-and-node/web-app-development-with-angular-and-node.component';
import { PwaWithAngularReactComponent } from './pwa-with-angular-react/pwa-with-angular-react.component';
import { AmazonWebServicesComponent } from './amazon-web-services/amazon-web-services.component';
import { GoogleCloudComponent } from './google-cloud/google-cloud.component';
import { AzureCloudServicesComponent } from './azure-cloud-services/azure-cloud-services.component';
import { DigitalOceanComponent } from './digital-ocean/digital-ocean.component';


@NgModule({
  declarations: [ServicesComponent, AndroidApplicationServiceComponent, IosApplicationServiceComponent, WebApplicationServiceComponent, UiUxServiceComponent, HybridMobileAppServiceComponent, AwsServiceComponent, DigitalMarketingServiceComponent, SocialMediaMarketingComponent, SearchEngineOptimizationComponent, SearchEngineMarketingComponent, ContentWritingComponent, MobileAppDevelopmentComponent, MobileAppsWithFlutterComponent, MobileAppDevelopmentWithReactComponent, LogoDesignComponent, UiUxForWebAppsComponent, UiUxForMobileAppsComponent, PrototypingComponent, WebAppDevelopmentWithPhpLaravelComponent, WebAppDevelopmentWithPythonDjangoComponent, WebAppDevelopmentWithAngularAndNodeComponent, PwaWithAngularReactComponent, AmazonWebServicesComponent, GoogleCloudComponent, AzureCloudServicesComponent, DigitalOceanComponent],
  imports: [
    CommonModule,
    ServicesRoutingModule,
    SharedModule
  ]
})
export class ServicesModule { }
