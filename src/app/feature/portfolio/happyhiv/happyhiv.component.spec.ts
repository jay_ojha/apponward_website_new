import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HappyhivComponent } from './happyhiv.component';

describe('HappyhivComponent', () => {
  let component: HappyhivComponent;
  let fixture: ComponentFixture<HappyhivComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HappyhivComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HappyhivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
