import {
  Component,
  OnInit,
  AfterViewInit,
  Injectable,
  Inject,
  HostListener,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-sharedare',
  templateUrl: './sharedare.component.html',
  styleUrls: ['./sharedare.component.css'],
})
export class SharedareComponent implements OnInit {
  title = 'Sharedare | Challenge Sharing App | Apponward';
  constructor(
    private titleService: Title,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private metaService: Meta
  ) {}

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'Sharedare is an android application developed by Apponward Technologies. It is the best dare app where users can share and accomplish dares of nearby locations.',
      },
      {
        name: 'og:image',
        content: 'https://nzjhgtms.apponward.com/assets/images/apponward.gif',
      },
      {
        name: 'keywords',
        content:
          'Sharedare Portfolio,Sharedare, Best dare App, Best Sharedare Android application, Android Application,  Apponward portfolio, Top App Development Company, Apponward Technologies',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'Sharedare is an android application developed by Apponward Technologies. It is the best dare app where users can share and accomplish dares of nearby locations.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    // debugger;
    // console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_btn', 'true');
    }
  }
}
