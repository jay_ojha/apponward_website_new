import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';


@Component({
  selector: 'app-hybrid-mobile-app-service',
  templateUrl: './hybrid-mobile-app-service.component.html',
  styleUrls: ['./hybrid-mobile-app-service.component.css']
})
export class HybridMobileAppServiceComponent implements OnInit {
  title = 'Navigating Hybrid Mobile App Development | Apponward ';
  constructor(private titleService: Title,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private metaService: Meta,) { }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {name: 'description', content: 'Apponward is building the best hybrid mobile app development. With our hybrid mobile app development, we can take your idea and make it a reality.'},
      {name: 'og:image', content: 'https://lh5.googleusercontent.com/p/AF1QipNMS4ebHVvp8eAXp1GzERiq-KsPMwiIp1CUnlq8=w203-h116-k-no'},
      {name: 'keywords', content: 'Hybrid App Development Company, Mobile App Development Company, Hybrid Application Development, Application Development Company, Mobile App Development Companies, Top Hybrid App Development Companies, Apponward Technologies'},
      {name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.'},
      {name: 'author', content: 'Apponward Technologies Pvt. Ltd.'},
      {name: 'robots', content: 'index, follow'},
      { name: 'twitter:card', content: 'Apponward is building the best hybrid mobile app development. With our hybrid mobile app development, we can take your idea and make it a reality.'},
      { name: 'twitter:site', content:''},
      { name: 'twitter:creator', content:''}
    ]);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
    sessionStorage.setItem('back_landing_section', 'hybridService');
    }
  }
}
