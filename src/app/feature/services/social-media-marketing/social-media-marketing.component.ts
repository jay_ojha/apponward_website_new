import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-social-media-marketing',
  templateUrl: './social-media-marketing.component.html',
  styleUrls: ['./social-media-marketing.component.css'],
})
export class SocialMediaMarketingComponent implements OnInit {
  title = 'Social Media Marketing | SMM | Apponward';
  constructor(
    private titleService: Title,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private metaService: Meta
  ) {}
  navigationFn() {
    if (isPlatformBrowser(this.platformId)) {
      let back_history = sessionStorage.getItem('back_landing_section');
      if (back_history == 'blogHome') {
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('blogHome')
            ? document.getElementById('blogHome')
            : '0';
          let h = testDiv.offsetTop;
          window.scrollTo(h - 600, h);
        }, 0); //5s
      }
    }
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'Create your online presence with social media marketing services of Apponward Technologies. We assist you in LinkedIn marketing, content planning and much more.',
      },
      {
        name: 'og:image',
        content:
          'https://lh5.googleusercontent.com/p/AF1QipNMS4ebHVvp8eAXp1GzERiq-KsPMwiIp1CUnlq8=w203-h116-k-no',
      },
      {
        name: 'keywords',
        content:
          'social media marketing, social media marketing agency, social media marketing strategy, social media marketing services, social media marketing companies, social media marketing for business, linkedin marketing, youtube marketing services, content planning for social media, apponward technologies',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'Create your online presence with social media marketing services of Apponward Technologies. We assist you in LinkedIn marketing, content planning and much more.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_landing_section', 'services');
    }
  }
}
