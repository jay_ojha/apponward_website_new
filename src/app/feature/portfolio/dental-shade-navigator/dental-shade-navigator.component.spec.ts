import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DentalShadeNavigatorComponent } from './dental-shade-navigator.component';

describe('DentalShadeNavigatorComponent', () => {
  let component: DentalShadeNavigatorComponent;
  let fixture: ComponentFixture<DentalShadeNavigatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DentalShadeNavigatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DentalShadeNavigatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
