import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-digital-marketing-service',
  templateUrl: './digital-marketing-service.component.html',
  styleUrls: ['./digital-marketing-service.component.css'],
})
export class DigitalMarketingServiceComponent implements OnInit {
  title = 'Best Digital Marketing Agency | Apponward ';
  constructor(
    private titleService: Title,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private metaService: Meta
  ) {}
  navigationFn() {
    if (isPlatformBrowser(this.platformId)) {
      let back_history = sessionStorage.getItem('back_landing_section');

      if (back_history == 'servicescontent') {
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('servicescontent')
            ? document.getElementById('servicescontent')
            : '0';
          let h = testDiv.offsetTop;
          window.scrollTo(h - 800, 1800);
        }, 0); //5s
      } else if (back_history == 'blogHome') {
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('blogHome')
            ? document.getElementById('blogHome')
            : '0';
          let h = testDiv.offsetTop;
          window.scrollTo(h - 600, h);
        }, 0); //5s
      }
    }
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'Apponward Technologies is a digital marketing company specializing in SEO, Google Ads services, SEM services, social media marketing, and much more.',
      },
      {
        name: 'og:image',
        content:
          'https://lh5.googleusercontent.com/p/AF1QipNMS4ebHVvp8eAXp1GzERiq-KsPMwiIp1CUnlq8=w203-h116-k-no',
      },
      {
        name: 'keywords',
        content:
          'Top Digital Marketing Companies, Digital Marketing Company, Best SEO Services, Best SMM Services, Google Ads Services, Facebook Ads Services, SEM Services, Apponward Technologies',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'Apponward Technologies is a digital marketing company specializing in SEO, Google Ads services, SEM services, social media marketing, and much more.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
    setTimeout(() => {
      this.navigationFn();
    }, 5000);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_landing_section', 'digiService');
    }
  }
}
