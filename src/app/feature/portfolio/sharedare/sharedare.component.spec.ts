import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedareComponent } from './sharedare.component';

describe('SharedareComponent', () => {
  let component: SharedareComponent;
  let fixture: ComponentFixture<SharedareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SharedareComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
