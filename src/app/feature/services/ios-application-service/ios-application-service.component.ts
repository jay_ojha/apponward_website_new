import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';


@Component({
  selector: 'app-ios-application-service',
  templateUrl: './ios-application-service.component.html',
  styleUrls: ['./ios-application-service.component.css']
})
export class IosApplicationServiceComponent implements OnInit {
  title = 'Best iOS App Development Company | Apponward';
  constructor(private titleService: Title,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private metaService: Meta,)  {
     
    }
    navigationFn(){
      if (isPlatformBrowser(this.platformId)) {
      let back_history = sessionStorage.getItem('back_landing_section');
      if(back_history == 'blogHome'){
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('blogHome')
            ? document.getElementById('blogHome')
            : '0';
          let h = testDiv.offsetTop;
          window.scrollTo(h - 600, h);
        }, 0); //5s
      }
    }
    }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {name: 'description', content: 'Apponward makes stunning, iOS apps that are fully responsive and easy to customize. We’re always looking for new ways to help you get the most of it.'},
      {name: 'og:image', content: 'https://lh5.googleusercontent.com/p/AF1QipNMS4ebHVvp8eAXp1GzERiq-KsPMwiIp1CUnlq8=w203-h116-k-no'},
      {name: 'keywords', content: 'iOS App Development Company, iOS App Development Company, iOS Application Development, Application Development Company, iOS App Development Companies, Top iOS App Development Companies, iOS Apps, Apponward Technologies'},
      {name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.'},
      {name: 'author', content: 'Apponward Technologies Pvt. Ltd.'},
      {name: 'robots', content: 'index, follow'},
      { name: 'twitter:card', content: 'Apponward makes stunning, iOS apps that are fully responsive and easy to customize. We’re always looking for new ways to help you get the most of it.'},
      { name: 'twitter:site', content:''},
      { name: 'twitter:creator', content:''}
    ]);
    setTimeout(() => {
      this.navigationFn();
    }, 5000);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    // debugger;
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
    sessionStorage.setItem('back_btn', 'true');
    }
  }
}
