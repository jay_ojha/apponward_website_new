import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebAppDevelopmentWithAngularAndNodeComponent } from './web-app-development-with-angular-and-node.component';

describe('WebAppDevelopmentWithAngularAndNodeComponent', () => {
  let component: WebAppDevelopmentWithAngularAndNodeComponent;
  let fixture: ComponentFixture<WebAppDevelopmentWithAngularAndNodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebAppDevelopmentWithAngularAndNodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebAppDevelopmentWithAngularAndNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
