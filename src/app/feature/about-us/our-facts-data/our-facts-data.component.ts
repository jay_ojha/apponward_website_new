import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-our-facts-data',
  templateUrl: './our-facts-data.component.html',
  styleUrls: ['./our-facts-data.component.css'],
})
export class OurFactsDataComponent implements OnInit {
  constructor(
    @Inject(PLATFORM_ID)
    private platformId: any
  ) {}

  ngOnInit(): void {}
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    debugger;
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_btn', 'true');
    }
  }
}
