import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileAppsWithFlutterComponent } from './mobile-apps-with-flutter.component';

describe('MobileAppsWithFlutterComponent', () => {
  let component: MobileAppsWithFlutterComponent;
  let fixture: ComponentFixture<MobileAppsWithFlutterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileAppsWithFlutterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileAppsWithFlutterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
