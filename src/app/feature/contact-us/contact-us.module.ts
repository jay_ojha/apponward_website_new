import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module'
import { ContactUsRoutingModule } from './contact-us-routing.module';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ToastrModule } from 'ngx-toastr';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularButtonLoaderModule } from 'angular-button-loader';
import { HttpClientModule } from '@angular/common/http';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';


@NgModule({
  declarations: [ContactUsComponent],
  imports: [
    CommonModule,
    ContactUsRoutingModule,
    SharedModule,
    ToastrModule.forRoot({timeOut: 5000}),
    ReactiveFormsModule,
    FormsModule,
    AngularButtonLoaderModule.forRoot(),
    HttpClientModule,
    NgxCaptchaModule,
    NgxIntlTelInputModule
    
  ]
})
export class ContactUsModule { }
