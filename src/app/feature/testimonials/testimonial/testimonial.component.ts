import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { ApponwardService } from 'src/app/_services/apponward.services';
import { OwlOptions } from 'ngx-owl-carousel-o';
@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.css'],
})
export class TestimonialComponent implements OnInit {
  title = 'Client Reviews and Testimonial |  Apponward';
  txtTesimonial: any = [];
  vdoTesimonial: any = [];
  customOptions: OwlOptions = {
    items: 2,
    loop: true,
    autoplay: false,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    mouseDrag: false,
    rewind: false,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    nav: true,
    navSpeed: 700,
    navText: [
      '<i class="bx bx-left-arrow-alt"></i>',
      '<i class="bx bx-right-arrow-alt"></i>',
    ],
    responsive: {
      0: {
        items: 1,
      },
      480: {
        items: 1,
      },
      767: {
        items: 2,
      },
      1024: {
        items: 2,
      },
      1600: {
        items: 2,
      },
    },
  };

  carouselBanners = {
    loop: true,
    margin: 0,
    autoplay: true,
    autoplayTimeout: 5000,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      480: {
        items: 2,
      },
      767: {
        items: 2,
      },
      992: {
        items: 2,
      },
      1200: {
        items: 2,
      },
    },
  };
  constructor(
    private titleService: Title,
    private metaService: Meta,
    private apponwardService: ApponwardService,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private toastrService: ToastrService
  ) {
    this.getTxtTestimonialData();
    this.getVdoTestimonialData();

  }

  navigationFn() {
    if (isPlatformBrowser(this.platformId)) {
      let back_history = sessionStorage.getItem('back_landing_section');
      if (back_history == 'blogHome') {
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('blogHome')
            ? document.getElementById('blogHome')
            : '0';
          let h = testDiv.offsetTop;
          window.scrollTo(h - 600, h);
        }, 0); //5s
      }
    }
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'We assist you in end to end app development. We build applications designed and developed by experts. Our testimonials reflects our sincere efforts.',
      },
      {
        name: 'og:image',
        content: 'https://nzjhgtms.apponward.com/assets/images/apponward.gif',
      },
      {
        name: 'keywords',
        content:
          'Testimonials, Client testimonials, Client services, App development, Web development, Top App Development Company, Apponward Technologies',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'We assist you in end to end app development. We build applications designed and developed by experts. Our testimonials reflects our sincere efforts.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
    setTimeout(() => {
      this.navigationFn();
    }, 5000);
  }
  getTxtTestimonialData() {
    this.apponwardService.getTxtTestimonail().subscribe(
      (data: any) => {
        this.txtTesimonial = data.result;
        // console.log(this.txtTesimonial);
      },
      (error) => {
        this.toastrService.error(error.error.message || 'Unknown error');
      }
    );
  }

  getVdoTestimonialData() {
    this.apponwardService.getVdoTestimonail().subscribe(
      (data: any) => {
        this.vdoTesimonial = data.result;
        // console.log(this.vdoTesimonial);
      },
      (error) => {
        this.toastrService.error(error.error.message || 'Unknown error');
      }
    );
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_landing_section', 'serviceTestimonial');
    }
  }
}
