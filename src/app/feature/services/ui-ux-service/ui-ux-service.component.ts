import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-ui-ux-service',
  templateUrl: './ui-ux-service.component.html',
  styleUrls: ['./ui-ux-service.component.css'],
})
export class UiUxServiceComponent implements OnInit {
  title = 'Mobile and Web App UI/UX Design Company | Apponward ';
  constructor(
    private titleService: Title,
    private metaService: Meta,
    @Inject(PLATFORM_ID)
    private platformId: any
  ) {}
  navigationFn() {
    if (isPlatformBrowser(this.platformId)) {
      let back_history = sessionStorage.getItem('back_landing_section');
      if (back_history == 'blogHome') {
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('blogHome')
            ? document.getElementById('blogHome')
            : '0';
          let h = testDiv.offsetTop;
          window.scrollTo(h - 600, h);
        }, 0); //5s
      }
    }
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'We are one of the top UI and UX companies. We are designing the future for the next gen and changing the way they interact with technology.',
      },
      {
        name: 'og:image',
        content: 'https://nzjhgtms.apponward.com/assets/images/apponward.gif',
      },
      {
        name: 'keywords',
        content:
          'Top UI and UX Companies, Best UI Company, Best UX Company, UI Services, UX Services, Application Development Company, Best UI and UX Company, Apponward Technologies',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'We are one of the top UI and UX companies. We are designing the future for the next gen and changing the way they interact with technology.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
    setTimeout(() => {
      this.navigationFn();
    }, 5000);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_landing_section', 'services');
    }
  }
}
