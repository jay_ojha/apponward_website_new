import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';
import {
  Router,
  ActivatedRoute,
  ParamMap,
  NavigationEnd,
} from '@angular/router';
import { ApponwardService } from 'src/app/_services/apponward.services';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-blog-description',
  templateUrl: './blog-description.component.html',
  styleUrls: ['./blog-description.component.css'],
})
export class BlogDescriptionComponent implements OnInit {
  title = 'Latest Blogs | Apponward';
  blog_id: any;
  blogDetails: any;
  blogCommentId: any;
  allCommentData: any;
  meta_title: any;
  meta_keywords: any;
  meta_description: any;
  blogCommentForm: any;
  isFormSubmitted: boolean = false;
  blogLists: any = [];
  totalRecords: any = [String];
  page: number = 1;
  constructor(
    private titleService: Title,
    private metaService: Meta,
    private apponwardService: ApponwardService,
    private formBuilder: FormBuilder,
    private router: Router,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private toastrService: ToastrService,
    private route: ActivatedRoute,
    private http: HttpClient
  ) {
    this.blog_id = this.route.snapshot.paramMap.get('title_url');
    console.log(this.blog_id);
    this.getSingleBlogDetails();
    this.getAllTagsInformation();
    this.getAllBlogList();
  }

  ngOnInit(): void {
    this.blogCommentForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      message: ['', [Validators.required]],
    });
  }
  get f() {
    //console.log(this.addSubscriberForm.controls);
    return this.blogCommentForm.controls;
  }

  onBlogCommentForm() {
    this.isFormSubmitted = true;
    if (this.blogCommentForm.invalid) {
      return;
    }
    console.log('form values=====', JSON.stringify(this.blogCommentForm.value));
    var formData: any = new FormData();
    formData.append('name', this.blogCommentForm.value.name);
    formData.append('email', this.blogCommentForm.value.email);
    formData.append('message', this.blogCommentForm.value.message);
    formData.append('blog_id', localStorage.getItem('blog_id'));

    // let url = `${environment.apiUrl}/contactUs`;
    let url = `https://admin.apponward.com/api/addComment`;
    console.log(url);
    let config = {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      responseType: 'blob',
    };
    this.http.post(url, formData).subscribe(
      (data: any) => {
        console.log('adata===============', url);
        console.log('data is here', data);
        this.toastrService.success(
          'Thanks for your commenting \n My Blog post.'
        );
        this.isFormSubmitted = false;
        this.router.navigate(['']);
        location.reload();
        // this.blogCommentForm.reset();
        // this.commentData("Blog_id");
      },
      (error) => {
        console.log('error', error);
        this.toastrService.error('Some error occured! please try again.');
      }
    );
  }

  getSingleBlogDetails() {
    this.apponwardService.getBlogDetail(this.blog_id).subscribe(
      (data: any) => {
        this.blogDetails = data.result;

        this.blogCommentId = parseInt(this.blogDetails.id);
        if (isPlatformBrowser(this.platformId)) {
          localStorage.setItem('blog_id', this.blogCommentId);
        }
        this.commentData(this.blogCommentId);

        this.meta_keywords = data.result.meta_keywords;
        this.meta_description = data.result.meta_description;
        setTimeout(() => {
          if (this.blogDetails.meta_title) {
            this.titleService.setTitle(this.blogDetails.meta_title);
          } else {
            this.titleService.setTitle(this.title);
          }
          this.metaService.removeTag("name='description'");
          this.metaService.removeTag("name='og:image'");
          this.metaService.removeTag("name='keywords'");
          this.metaService.removeTag("name='ogtag'");
          this.metaService.removeTag("name='author'");
          this.metaService.removeTag("name='robots'");
          this.metaService.removeTag("name='twitter:site'");
          this.metaService.removeTag("name='twitter:creator'");
          this.metaService.addTags([
            {
              name: 'description',
              content: this.meta_description
                ? this.meta_description
                : 'We are one of the leading app development companies in Germany, UK & India for Startups Enterprise, focus on high-quality android, ios web development services',
            },
            {
              name: 'og:image',
              content:
                'https://lh5.googleusercontent.com/p/AF1QipNMS4ebHVvp8eAXp1GzERiq-KsPMwiIp1CUnlq8=w203-h116-k-no',
            },
            {
              name: 'keywords',
              content: this.meta_keywords
                ? this.meta_keywords
                : 'Android App Development Company, Android App Development Company in Noida, Android App Development Company in India, Android App Development, Android app development India, Android App Development Services, Android Application Development Company, Android App Developers, Custom Android App Development',
            },
            { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
            { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
            { name: 'robots', content: 'index, follow' },
          ]);
        }, 1000);
      },
      (error) => {
        this.toastrService.error(error.error.message || 'Unknown error');
      }
    );
  }
  commentData(BlogId: any) {
    this.apponwardService.getBlogComment(BlogId).subscribe(
      (data: any) => {
        this.allCommentData = data.data;
        console.log('Comment Data', this.allCommentData);
      },
      (error) => {
        this.toastrService.error(error.error.message || 'Unknown error');
      }
    );
  }
  getAllTagsInformation() {
    // this.metaService.addTags([
    //   // {name: 'description', content: this.meta_description},
    //   {name: 'og:image', content: 'https://lh5.googleusercontent.com/p/AF1QipNMS4ebHVvp8eAXp1GzERiq-KsPMwiIp1CUnlq8=w203-h116-k-no'},
    //   // {name: 'keywords', content: this.meta_keywords},
    //   {name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.'},
    //   {name: 'author', content: 'Apponward Technologies Pvt. Ltd.'},
    //   {name: 'robots', content: 'index, follow'}
    // ]);
  }
  getAllBlogList() {
    this.apponwardService.getBlogList().subscribe(
      (data: any) => {
        this.blogLists = data.result.slice(0, 1);
        console.log(this.blogLists);
        this.totalRecords = data.result.length;
        console.log(this.totalRecords);
      },
      (error) => {
        this.toastrService.error(error.error.message || 'Unknown error');
      }
    );
  }
  viewDetails(id: any) {
    console.log(id);
    this.router.navigate(['/blogs', id]);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    console.log('Back button pressed');
    // debugger;
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_btn', 'true');
    }
  }
}
