import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { Location } from '@angular/common';
// I also import Router so that I can subscribe to events
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApponwardService } from './_services/apponward.services';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'apponward';
  name = 'ngx-sharebuttons';
  route: any;
  responseStatus: any;
  currentUrl: any;
  constructor(
    location: Location,
    private router: Router,
    private toastrService: ToastrService,
    private apponwardService: ApponwardService,
    @Inject(PLATFORM_ID)
    private platformId: any
  ) {
    // console.log("window.location.href",window.location.href);
    this.currentUrl = window.location.href;
    // console.log("CurrentUrl",this.currentUrl);
    let reqData = {
      url: this.currentUrl,
    };
    if (isPlatformBrowser(this.platformId)) {
      this.apponwardService.getValidateUrl(reqData).subscribe(
        (data: any) => {
          // console.log("data for route");
          this.responseStatus = data;
          if (this.responseStatus.status == true) {
            // console.log("his.responseStatus.status",this.responseStatus.status);
          } else {
            // this.router.navigate(['']);
          }
          //  console.log(this.responseStatus);
        },
        (error) => {
          // this.toastrService.error(error.error.message || 'Unknown error');
        }
      );
    }
  }
  ngOnInit(): void {}
}
