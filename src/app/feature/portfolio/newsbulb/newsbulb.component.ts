import {
  Component,
  OnInit,
  AfterViewInit,
  Injectable,
  Inject,
  HostListener,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-newsbulb',
  templateUrl: './newsbulb.component.html',
  styleUrls: ['./newsbulb.component.css']
})
export class NewsbulbComponent implements OnInit {

  title = 'Newsbulb | Best News App | Apponward ';
constructor(private titleService: Title,
  @Inject(PLATFORM_ID)
    private platformId: any,
  private metaService: Meta,) {

   }

ngOnInit(): void {
  this.titleService.setTitle(this.title);
  this.metaService.removeTag("name='description'");
  this.metaService.removeTag("name='og:image'");
  this.metaService.removeTag("name='keywords'");
  this.metaService.removeTag("name='ogtag'");
  this.metaService.removeTag("name='author'");
  this.metaService.removeTag("name='robots'");
  this.metaService.removeTag("name='twitter:site'");
  this.metaService.removeTag("name='twitter:creator'");
  this.metaService.addTags([
    {name: 'description', content: 'News Bulb is the best app for news. Users can read and listen to the top news using the News Bulb app.It also shows the news as per the preference of the users.'},
    {name: 'og:image', content: 'https://nzjhgtms.apponward.com/assets/images/apponward.gif'},
    {name: 'keywords', content: 'News Bulb, Portfolio, News Bulb, Best News Bulb App, App for News, Best NEWS App, Best App Development Company,  Apponward portfolio, Top App Development Company, Apponward Technologies'},
    {name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.'},
    {name: 'author', content: 'Apponward Technologies Pvt. Ltd.'},
    {name: 'robots', content: 'index, follow'},
    { name: 'twitter:card', content: 'News Bulb is the best app for news. Users can read and listen to the top news using the News Bulb app.It also shows the news as per the preference of the users.'},
    { name: 'twitter:site', content:''},
    { name: 'twitter:creator', content:''}
  ]);
}
@HostListener('window:popstate', ['$event'])
  onPopState() {
    // debugger;
    // console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
    sessionStorage.setItem('back_btn', 'true');
    }
  }

}
