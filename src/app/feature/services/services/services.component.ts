import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
})
export class ServicesComponent implements OnInit {
  title = 'Top Notch Mobile and Web App Development Services | Apponward';
  constructor(
    private titleService: Title,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private metaService: Meta
  ) {}
  navigationFn() {
    if (isPlatformBrowser(this.platformId)) {
      let back_history = sessionStorage.getItem('back_landing_section');
      if (back_history == 'hybridService') {
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('hybridService')
            ? document.getElementById('hybridService')
            : '0';
          // debugger;
          console.log(testDiv);
          let h = testDiv.offsetTop;
          window.scrollTo(h, 3000);
        }, 0); //5s
      } else if (back_history == 'cloudService') {
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('cloudService')
            ? document.getElementById('cloudService')
            : '0';
          let h = testDiv.offsetTop;
          window.scrollTo(h, 3500);
        }, 0); //5s
      } else if (back_history == 'digiService') {
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('digiService')
            ? document.getElementById('digiService')
            : '0';
          let h = testDiv.offsetTop;
          window.scrollTo(h, 4200);
        }, 0); //5s
      } else if (back_history == 'blogHome') {
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('blogHome')
            ? document.getElementById('blogHome')
            : '0';
          let h = testDiv.offsetTop;
          window.scrollTo(h - 600, h);
        }, 0); //5s
      }
    }
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'We transform the ideas of our clients into apps to increase their revenue and profitability. Apponward is making IT services easy to buy, use, and manage.',
      },
      {
        name: 'og:image',
        content: 'https://nzjhgtms.apponward.com/assets/images/apponward.gif',
      },
      {
        name: 'keywords',
        content:
          'Mobile App Development Services, Web App Development Services, Best App Developer, Custom Mobile App Development Services, Best Mobile App Design Services, Top Apps Making Company',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'We transform the ideas of our clients into apps to increase their revenue and profitability. Apponward is making IT services easy to buy, use, and manage.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
    setTimeout(() => {
      this.navigationFn();
    }, 5000);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_landing_section', 'services');
    }
  }
}
