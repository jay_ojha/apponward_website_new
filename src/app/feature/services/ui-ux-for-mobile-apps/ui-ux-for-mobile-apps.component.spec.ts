import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiUxForMobileAppsComponent } from './ui-ux-for-mobile-apps.component';

describe('UiUxForMobileAppsComponent', () => {
  let component: UiUxForMobileAppsComponent;
  let fixture: ComponentFixture<UiUxForMobileAppsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiUxForMobileAppsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UiUxForMobileAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
