import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsbulbComponent } from './newsbulb.component';

describe('NewsbulbComponent', () => {
  let component: NewsbulbComponent;
  let fixture: ComponentFixture<NewsbulbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsbulbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsbulbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
