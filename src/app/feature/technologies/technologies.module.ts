import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { TechnologiesRoutingModule } from './technologies-routing.module';
import { TechnologyComponent } from './technology/technology.component';


@NgModule({
  declarations: [TechnologyComponent],
  imports: [
    CommonModule,
    TechnologiesRoutingModule,
    SharedModule
  ]
})
export class TechnologiesModule { }
