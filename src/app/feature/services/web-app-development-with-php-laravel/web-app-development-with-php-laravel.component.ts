import { Component, OnInit,HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID, } from '@angular/core';
  import { isPlatformBrowser } from '@angular/common';
  import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-web-app-development-with-php-laravel',
  templateUrl: './web-app-development-with-php-laravel.component.html',
  styleUrls: ['./web-app-development-with-php-laravel.component.css']
})
export class WebAppDevelopmentWithPhpLaravelComponent implements OnInit {
  title = 'Best Web Apps with PHP/Laravel - Apponward';

  constructor(
    private titleService: Title,
    private metaService: Meta
  ) {}

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'Apponward is a one-stop destination for all your web app development with php needs. We give the best web app development using Laravel thanks to a team of experienced specialists.',
      },
      {
        name: 'og:image',
        content:
          'https://lh5.googleusercontent.com/p/AF1QipNMS4ebHVvp8eAXp1GzERiq-KsPMwiIp1CUnlq8=w203-h116-k-no',
      },
      {
        name: 'keywords',
        content:
          'web app development using php web app development with php, Web App Development with Laravel,  Web App Development, Web Development,   web development with laravel,  web development using laravel,  modern web development with laravel, web application development with laravel php framework, web development tools laravel,  web application development with php, web development tools php, web application development with php and frameworks, web development projects using php,  web development projects in php',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'Apponward is a one-stop destination for all your web app development with php needs. We give the best web app development using Laravel thanks to a team of experienced specialists.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
  }

}
