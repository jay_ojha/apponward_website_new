import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeyfanComponent } from './heyfan.component';

describe('HeyfanComponent', () => {
  let component: HeyfanComponent;
  let fixture: ComponentFixture<HeyfanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeyfanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeyfanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
