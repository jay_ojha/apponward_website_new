import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoffeeKarmaComponent } from './coffee-karma.component';

describe('CoffeeKarmaComponent', () => {
  let component: CoffeeKarmaComponent;
  let fixture: ComponentFixture<CoffeeKarmaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoffeeKarmaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoffeeKarmaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
