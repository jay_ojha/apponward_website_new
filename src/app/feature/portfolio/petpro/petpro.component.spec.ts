import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetproComponent } from './petpro.component';

describe('PetproComponent', () => {
  let component: PetproComponent;
  let fixture: ComponentFixture<PetproComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PetproComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PetproComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
