import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NukkadPeHaiComponent } from './nukkad-pe-hai.component';

describe('NukkadPeHaiComponent', () => {
  let component: NukkadPeHaiComponent;
  let fixture: ComponentFixture<NukkadPeHaiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NukkadPeHaiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NukkadPeHaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
