import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { MaetrixComponent } from './maetrix/maetrix.component';
import { DentalShadeNavigatorComponent} from './dental-shade-navigator/dental-shade-navigator.component';
import { NukkadPeHaiComponent} from './nukkad-pe-hai/nukkad-pe-hai.component';
import { MeriNavigationComponent} from './meri-navigation/meri-navigation.component';
import { NewsbulbComponent} from './newsbulb/newsbulb.component';
import { HealthPlusComponent} from './health-plus/health-plus.component';
import { HeyfanComponent} from './heyfan/heyfan.component';
import { FootyedgeComponent} from './footyedge/footyedge.component';
import { FlowmixComponent} from './flowmix/flowmix.component';
import { SharedareComponent} from './sharedare/sharedare.component';
import { HappyhivComponent } from './happyhiv/happyhiv.component';
import { NoolComponent } from './nool/nool.component';
import { CoffeeKarmaComponent } from './coffee-karma/coffee-karma.component';
import { ProphecyComponent} from './prophecy/prophecy.component';
import { PetproComponent } from './petpro/petpro.component'; 


const routes: Routes = [
  { path: 'portfolio', component: PortfolioComponent },
  { path: 'portfolio/the-maetrix', component: MaetrixComponent },
  { path: 'portfolio/dental-shade-navigator', component: DentalShadeNavigatorComponent },
  { path: 'portfolio/nukkad-pe-hai', component: NukkadPeHaiComponent},
  { path: 'portfolio/meri-navigation' , component: MeriNavigationComponent},
  { path: 'portfolio/newsbulb', component: NewsbulbComponent},
  { path: 'portfolio/health-plus', component: HealthPlusComponent},
  { path: 'portfolio/heyfan', component: HeyfanComponent},
  { path: 'portfolio/footyedge', component: FootyedgeComponent},
  { path: 'portfolio/flowmixx', component: FlowmixComponent},
  { path: 'portfolio/sharedare', component: SharedareComponent},
  { path: 'portfolio/happyhiv', component: HappyhivComponent},
  { path: 'portfolio/nool', component: NoolComponent},
  { path: 'portfolio/coffee-karma', component: CoffeeKarmaComponent},
  { path: 'portfolio/prophecy', component: ProphecyComponent},
  { path: 'portfolio/petpro', component: PetproComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortfolioRoutingModule { }
