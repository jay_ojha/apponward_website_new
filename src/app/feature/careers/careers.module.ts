import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { CareersRoutingModule } from './careers-routing.module';
import { CareerComponent } from './career/career.component';
import { CareerDetailsComponent } from './career-details/career-details.component';
import { ToastrModule } from 'ngx-toastr';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularButtonLoaderModule } from 'angular-button-loader';
import { HttpClientModule } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})

export class TruncatePipe implements PipeTransform {
  transform(value: string, args: any[]): string {
      const limit = args.length > 0 ? parseInt(args[0], 10) : 20;
      const trail = args.length > 1 ? args[1] : '...';
      return value.length > limit ? value.substring(0, limit) + trail : value;
     }
  }

@NgModule({
  declarations: [CareerComponent, CareerDetailsComponent,TruncatePipe],
  imports: [
    CommonModule,
    CareersRoutingModule,
    SharedModule,
    ToastrModule.forRoot({timeOut: 5000}),
    ReactiveFormsModule,
    FormsModule,
    AngularButtonLoaderModule.forRoot(),
    HttpClientModule,
  ]
})
export class CareersModule { }
