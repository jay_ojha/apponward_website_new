import { NgModule,Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeBannerComponent } from './home-banner/home-banner.component';
import { HowWeWorkComponent } from './how-we-work/how-we-work.component';
import { HomePortfolioComponent } from './home-portfolio/home-portfolio.component';
import { HomeTestimonailComponent } from './home-testimonail/home-testimonail.component';
import { OurPresenceComponent } from './our-presence/our-presence.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { OurServicesComponent } from './our-services/our-services.component';
import { OurFactsDataComponent } from './our-facts-data/our-facts-data.component';
import { ToastrModule } from 'ngx-toastr';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularButtonLoaderModule } from 'angular-button-loader';
import { HttpClientModule } from '@angular/common/http';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { LazyLoadImageModule } from 'ng-lazyload-image'; 

@Pipe({
  name: 'truncate'
 })

 export class TruncatePipe implements PipeTransform {
  transform(value: string, args: any[]): string {
      const limit = args.length > 0 ? parseInt(args[0], 10) : 20;
      const trail = args.length > 1 ? args[1] : '...';
      return value.length > limit ? value.substring(0, limit) + trail : value;
     }
  }

@NgModule({
  declarations: [HomeComponent,HomeBannerComponent, HowWeWorkComponent, HomePortfolioComponent, HomeTestimonailComponent, OurPresenceComponent,TruncatePipe, PageNotFoundComponent, OurServicesComponent, OurFactsDataComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    CarouselModule,
    ToastrModule.forRoot({timeOut: 5000}),
    ReactiveFormsModule,
    FormsModule,
    AngularButtonLoaderModule.forRoot(),
    HttpClientModule,
    NgxIntlTelInputModule,
    LazyLoadImageModule
  ]
})
export class HomeModule { }
