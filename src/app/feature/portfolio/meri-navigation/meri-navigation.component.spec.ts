import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeriNavigationComponent } from './meri-navigation.component';

describe('MeriNavigationComponent', () => {
  let component: MeriNavigationComponent;
  let fixture: ComponentFixture<MeriNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeriNavigationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeriNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
