import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurFactsDataComponent } from './our-facts-data.component';

describe('OurFactsDataComponent', () => {
  let component: OurFactsDataComponent;
  let fixture: ComponentFixture<OurFactsDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurFactsDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurFactsDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
