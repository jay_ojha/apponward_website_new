import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { PortfolioRoutingModule } from './portfolio-routing.module';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { MaetrixComponent } from './maetrix/maetrix.component';
import { DentalShadeNavigatorComponent } from './dental-shade-navigator/dental-shade-navigator.component';
import { NukkadPeHaiComponent } from './nukkad-pe-hai/nukkad-pe-hai.component';
import { MeriNavigationComponent } from './meri-navigation/meri-navigation.component';
import { NewsbulbComponent } from './newsbulb/newsbulb.component';
import { HealthPlusComponent } from './health-plus/health-plus.component';
import { HeyfanComponent } from './heyfan/heyfan.component';
import { FootyedgeComponent } from './footyedge/footyedge.component';
import { SharedareComponent } from './sharedare/sharedare.component';
import { FlowmixComponent } from './flowmix/flowmix.component';
import { HappyhivComponent } from './happyhiv/happyhiv.component';
import { NoolComponent } from './nool/nool.component';
import { CoffeeKarmaComponent } from './coffee-karma/coffee-karma.component';
import { ProphecyComponent } from './prophecy/prophecy.component';
import { PetproComponent } from './petpro/petpro.component';


@NgModule({
  declarations: [PortfolioComponent, MaetrixComponent, DentalShadeNavigatorComponent, NukkadPeHaiComponent, MeriNavigationComponent, NewsbulbComponent, HealthPlusComponent, HeyfanComponent, FootyedgeComponent, SharedareComponent, FlowmixComponent, HappyhivComponent, NoolComponent, CoffeeKarmaComponent, ProphecyComponent, PetproComponent],
  imports: [
    CommonModule,
    PortfolioRoutingModule,
    SharedModule
  ]
})
export class PortfolioModule { }
