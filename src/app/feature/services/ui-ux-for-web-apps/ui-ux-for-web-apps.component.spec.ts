import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiUxForWebAppsComponent } from './ui-ux-for-web-apps.component';

describe('UiUxForWebAppsComponent', () => {
  let component: UiUxForWebAppsComponent;
  let fixture: ComponentFixture<UiUxForWebAppsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiUxForWebAppsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UiUxForWebAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
