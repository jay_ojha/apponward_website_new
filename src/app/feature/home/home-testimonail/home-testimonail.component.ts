import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { ApponwardService } from 'src/app/_services/apponward.services';
import { OwlOptions } from 'ngx-owl-carousel-o';
@Component({
  selector: 'app-home-testimonail',
  templateUrl: './home-testimonail.component.html',
  styleUrls: ['./home-testimonail.component.css'],
})
export class HomeTestimonailComponent implements OnInit {
  isBrowser:any;
  /*txtTesimonial:any=[];*/
  vdoTesimonial: any = [];
  hometxtTestimonial: any = [];
  constructor(
    private apponwardService: ApponwardService,
    private toastrService: ToastrService,
    @Inject(PLATFORM_ID)
    private platformId: any
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
    /*this.getTxtTestimonialData();*/
    this.getVdoTestimonialData();
    this.getHomeTxtTestimonialData();
  }

  ngOnInit(): void {}
  /*getTxtTestimonialData(){
    this.apponwardService.getTxtTestimonail().subscribe(
      (data: any) => {
        this.txtTesimonial=data.result.slice(1);
        console.log(this.txtTesimonial);
      },
      error => {
        this.toastrService.error(error.error.message || "Unknown error");
      }
    );
  }*/
  getHomeTxtTestimonialData() {
    this.apponwardService.getHomeTxtTestimonail().subscribe(
      (data: any) => {
      

        this.hometxtTestimonial = data.result;
     
      },
      (error) => {
        this.toastrService.error(error.error.message || 'Unknown error');
      }
    );
  }

  getVdoTestimonialData() {
    this.apponwardService.getVdoTestimonail().subscribe(
      (data: any) => {
        this.vdoTesimonial = data.result.slice(0, 3);
       
      },
      (error) => {
        this.toastrService.error(error.error.message || 'Unknown error');
      }
    );
  }
  customOptions: OwlOptions = {
    loop: true,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    nav: false,
    navSpeed: 100,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
  };
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    // debugger;
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_testimonialHome', 'true');
    }
  }
}
