import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebApplicationServiceComponent } from './web-application-service.component';

describe('WebApplicationServiceComponent', () => {
  let component: WebApplicationServiceComponent;
  let fixture: ComponentFixture<WebApplicationServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebApplicationServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebApplicationServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
