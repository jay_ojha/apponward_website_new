import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileAppDevelopmentWithReactComponent } from './mobile-app-development-with-react.component';

describe('MobileAppDevelopmentWithReactComponent', () => {
  let component: MobileAppDevelopmentWithReactComponent;
  let fixture: ComponentFixture<MobileAppDevelopmentWithReactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileAppDevelopmentWithReactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileAppDevelopmentWithReactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
