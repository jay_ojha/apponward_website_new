import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IosApplicationServiceComponent } from './ios-application-service.component';

describe('IosApplicationServiceComponent', () => {
  let component: IosApplicationServiceComponent;
  let fixture: ComponentFixture<IosApplicationServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IosApplicationServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IosApplicationServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
