import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { from } from 'rxjs';
import { HomeModule } from './feature/home/home.module';
import { AboutUsModule } from './feature/about-us/about-us.module';
import { OurTeamModule } from './feature/our-team/our-team.module';
import { ContactUsModule } from './feature/contact-us/contact-us.module'
import { ServicesModule } from './feature/services/services.module';
import { TechnologiesModule } from './feature/technologies/technologies.module';
import { TestimonialsModule } from './feature/testimonials/testimonials.module';
import { GalleryModule } from './feature/gallery/gallery.module';
import { CareersModule } from './feature/careers/careers.module';
import { PortfolioModule } from './feature/portfolio/portfolio.module';
import { BlogModule } from './feature/blog/blog.module';
import {PageNotFoundComponent} from './feature/home/page-not-found/page-not-found.component';
import { HomeComponent } from './feature/home/home/home.component';
const routes: Routes = [
  { path: '', loadChildren:'./feature/home/home.module#HomeModule' },
  { path: '', loadChildren:'./feature/about-us/about-us.module#AboutUsModule' },
  { path: '', loadChildren:'./feature/contact-us/contact-us.module#ContactUsModule' },
  { path: '', loadChildren:'./feature/services/services.module#ServicesModule' },
  { path: '', loadChildren:'./feature/technologies/technologies.module#TechnologiesModule' },
  { path: '', loadChildren:'./feature/testimonials/testimonials.module#TestimonialsModule' },
  { path: '', loadChildren:'./feature/gallery/gallery.module#GalleryModule' },
  { path: '', loadChildren:'./feature/careers/careers.module#CareersModule' },
  { path: '', loadChildren:'./feature/portfolio/portfolio.module#PortfolioModule'},
  { path: '', loadChildren:'./feature/blog/blog.module#BlogModule'},
  { path: '404', loadChildren:'./feature/page-not-found/page-not-found.component'},
  { path: '**', pathMatch: 'full', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    scrollPositionRestoration:'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
