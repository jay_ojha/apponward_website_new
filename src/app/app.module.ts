import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ToastrModule } from 'ngx-toastr';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularButtonLoaderModule } from 'angular-button-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Pipe, PipeTransform } from '@angular/core';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { NgxCaptchaModule } from 'ngx-captcha';
import {NgxPaginationModule} from 'ngx-pagination';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { NgxSocialShareModule } from 'ngx-social-share';
// import {Location,HashLocationStrategy, LocationStrategy, PathLocationStrategy} from '@angular/common';

@Pipe({
  name: 'truncate'
})

export class TruncatePipe implements PipeTransform {
  transform(value: string, args: any[]): string {
      const limit = args.length > 0 ? parseInt(args[0], 10) : 20;
      const trail = args.length > 1 ? args[1] : '...';
      return value.length > limit ? value.substring(0, limit) + trail : value;
     }
  }

@NgModule({
  declarations: [
    AppComponent,
    TruncatePipe
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    SharedModule,
    ToastrModule.forRoot({timeOut:10000}),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularButtonLoaderModule.forRoot(),
    BrowserAnimationsModule,
    CarouselModule,
    NgxCaptchaModule,
    NgxPaginationModule,
    NgxIntlTelInputModule,
    NgxSocialShareModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
