import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiUxServiceComponent } from './ui-ux-service.component';

describe('UiUxServiceComponent', () => {
  let component: UiUxServiceComponent;
  let fixture: ComponentFixture<UiUxServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiUxServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UiUxServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
