import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { TestimonialsRoutingModule } from './testimonials-routing.module';
import { TestimonialComponent } from './testimonial/testimonial.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
@NgModule({
  declarations: [TestimonialComponent],
  imports: [
    CommonModule,
    TestimonialsRoutingModule,
    SharedModule,
    CarouselModule
  ]
})
export class TestimonialsModule { }
