import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module'
import { OurTeamRoutingModule } from './our-team-routing.module';
import { TeamComponent } from './team/team.component';
import { CoreTeamComponent } from './core-team/core-team.component';
import { EmployeeTeamComponent } from './employee-team/employee-team.component';


@NgModule({
  declarations: [TeamComponent, CoreTeamComponent, EmployeeTeamComponent],
  imports: [
    CommonModule,
    OurTeamRoutingModule,
    SharedModule
  ]
})
export class OurTeamModule { }
