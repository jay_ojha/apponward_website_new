import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AndroidApplicationServiceComponent } from './android-application-service.component';

describe('AndroidApplicationServiceComponent', () => {
  let component: AndroidApplicationServiceComponent;
  let fixture: ComponentFixture<AndroidApplicationServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AndroidApplicationServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AndroidApplicationServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
