import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
  Renderer2,
} from '@angular/core';
import { isPlatformBrowser, DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-mobile-app-development',
  templateUrl: './mobile-app-development.component.html',
  styleUrls: ['./mobile-app-development.component.css'],
})
export class MobileAppDevelopmentComponent implements OnInit {
  constructor(
    @Inject(PLATFORM_ID)
    private platformId: any,
    private _renderer2: Renderer2,
    @Inject(DOCUMENT) private _document: Document,
  ) {}
  navigationFn() {
    if (isPlatformBrowser(this.platformId)) {
      let back_history = sessionStorage.getItem('back_landing_section');
      if (back_history == 'blogHome') {
        sessionStorage.clear();
        setTimeout(() => {
          let testDiv: any = document.getElementById('blogHome')
            ? document.getElementById('blogHome')
            : '0';
          let h = testDiv.offsetTop;
          window.scrollTo(h - 600, h);
        }, 0); //5s
      }
    }
    setTimeout(() => {
      this.navigationFn();
    }, 5000);
  }

  public ngOnInit(): void {
  let script = this._renderer2.createElement('script');
  script.type = `application/ld+json`;
  script.text = `
  {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "mainEntity": [{
      "@type": "Question",
      "name": "How do I start mobile app development?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "1. Create a roadmap for the app.
  2. Conduct competitive market analysis.
  3. Create a list of the app features.
  4. Mock out the app designs.
  5. Create the visual design for app.
  6. Put the marketing plan together.
  7. Build app and upload it to the App/Play store.
  8. Maintain the app throughout its lifecycle.
  
  Alternatively, you can simply share your idea and requirements with us, and we'll take care of the rest."
      }
    },{
      "@type": "Question",
      "name": "What does a mobile app development company do?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "A mobile app development company offers a variety of mobile app development services, including iOS and Android app development, cross-platform app development, user experience design, and incorporating innovative mobile interfaces like chat and voice."
      }
    },{
      "@type": "Question",
      "name": "What are the different types of mobile apps?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "There are three major types of mobile apps:
  1. Native applications
  2. Web-based mobile apps 
  3. Hybrid apps
  
  Hybrid mobile applications integrate native and web-based app features. Native apps cover Android, iOS and Windows."
      }
    },{
      "@type": "Question",
      "name": "What does your app development process look like?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "The apps are developed using the AGILE scrum methodology. We follow the most adaptable methodology which is suitable for both the client and the project. From requirement gathering to app maintenance, we assist our clients throughout the process."
      }
    },{
      "@type": "Question",
      "name": "Why should I hire you for mobile app development?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Apponward is one of the leading mobile app development businesses, with over three years of expertise. We've delivered 75+ mobile applications and digital transformation solutions to several enterprises across the world."
      }
    },{
      "@type": "Question",
      "name": "I want to keep my Android app idea confidential. Will you sign an NDA with me?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes. We abide by all IT regulations and agree to sign an NDA."
      }
    }]
  }
`;
 
  this._renderer2.appendChild(this._document.body, script);
}
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_landing_section', 'services');
    }
  }
}
