import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitalOceanComponent } from './digital-ocean.component';

describe('DigitalOceanComponent', () => {
  let component: DigitalOceanComponent;
  let fixture: ComponentFixture<DigitalOceanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DigitalOceanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitalOceanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
