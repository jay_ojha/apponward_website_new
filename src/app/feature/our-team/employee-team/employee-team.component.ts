import {
  Component,
  OnInit,
  AfterViewInit,
  Injectable,
  Inject,
  HostListener,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { ApponwardService } from 'src/app/_services/apponward.services';
@Component({
  selector: 'app-employee-team',
  templateUrl: './employee-team.component.html',
  styleUrls: ['./employee-team.component.css'],
})
export class EmployeeTeamComponent implements OnInit {
  empData: any = [];
  constructor(
    private apponwardService: ApponwardService,
    private toastrService: ToastrService,
    @Inject(PLATFORM_ID)
    private platformId: any
  ) {
    this.getEmployeesData();
  }

  ngOnInit(): void {}
  getEmployeesData() {
    this.apponwardService.getTeam().subscribe(
      (data: any) => {
        this.empData = data.result;
        console.log(this.empData);
      },
      (error) => {
        this.toastrService.error(error.error.message || 'Unknown error');
      }
    );
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    // debugger;
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_btn', 'true');
    }
  }
}
