import { Component, OnInit } from '@angular/core';

import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-pwa-with-angular-react',
  templateUrl: './pwa-with-angular-react.component.html',
  styleUrls: ['./pwa-with-angular-react.component.css']
})
export class PwaWithAngularReactComponent implements OnInit {

  title = 'Best PWA with Angular/React - Apponward';

  constructor(private titleService: Title,
    private metaService: Meta) { }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'Apponward helps you transform your ideas into progressive web apps . We offer latest progressive web apps with react along with several other features and frameworks.',
      },
      {
        name: 'og:image',
        content:
          'https://lh5.googleusercontent.com/p/AF1QipNMS4ebHVvp8eAXp1GzERiq-KsPMwiIp1CUnlq8=w203-h116-k-no',
      },
      {
        name: 'keywords',
        content:
          'progressive web apps, what are progressive web apps, progressive web apps examples,  progressive web apps with react, progressive web apps development progressive web apps reactjs, progressive web apps angular, progressive web apps vs native app,  progressive web apps features, progressive web apps framework,  progressive web apps on ios, progressive web apps app store, progressive web apps wordpress',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'Apponward helps you transform your ideas into progressive web apps . We offer latest progressive web apps with react along with several other features and frameworks.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
  }

}
