import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoolComponent } from './nool.component';

describe('NoolComponent', () => {
  let component: NoolComponent;
  let fixture: ComponentFixture<NoolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
