import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FootyedgeComponent } from './footyedge.component';

describe('FootyedgeComponent', () => {
  let component: FootyedgeComponent;
  let fixture: ComponentFixture<FootyedgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FootyedgeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FootyedgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
