import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';
import { ApponwardService } from 'src/app/_services/apponward.services';
import { ToastrService } from 'ngx-toastr';
import {
  Router,
  ActivatedRoute,
  ParamMap,
  NavigationEnd,
} from '@angular/router';
@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css'],
})
export class BlogsComponent implements OnInit {
  title = 'Latest Blogs | Apponward';
  blogLists: any = [];
  blogDetails: any = [];
  totalRecords: any = [String];
  page: number = 1;
  constructor(
    private titleService: Title,
    private metaService: Meta,
    private apponwardService: ApponwardService,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private toastrService: ToastrService,
    private router: Router
  ) {
    this.getAllBlogList();
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'We are the IT company that helps create more value from data. Stay updated with the technologies and trends in IT with our latest blogs.',
      },
      {
        name: 'og:image',
        content: 'https://nzjhgtms.apponward.com/assets/images/apponward.gif',
      },
      {
        name: 'keywords',
        content:
          'Blogs, Technology Blogs, Top Blogs in Digital Marketing, New Technology, Top Web App Development Company, Apponward Technologies',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'We are the IT company that helps create more value from data. Stay updated with the technologies and trends in IT with our latest blogs.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
  }
  getAllBlogList() {
    this.apponwardService.getBlogList().subscribe(
      (data: any) => {
        this.blogLists = data.result;
        this.totalRecords = data.result.length;
      
      },
      (error) => {
        this.toastrService.error(error.error.message || 'Unknown error');
      }
    );
  }
  viewDetails(id: any) {
    console.log(id);
    this.router.navigate(['/blogs', id]);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    if (isPlatformBrowser(this.platformId)) {
      sessionStorage.setItem('back_landing_section', 'blogHome');
    }
  }
}
