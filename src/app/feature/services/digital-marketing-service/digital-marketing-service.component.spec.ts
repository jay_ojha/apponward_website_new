import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitalMarketingServiceComponent } from './digital-marketing-service.component';

describe('DigitalMarketingServiceComponent', () => {
  let component: DigitalMarketingServiceComponent;
  let fixture: ComponentFixture<DigitalMarketingServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DigitalMarketingServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitalMarketingServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
