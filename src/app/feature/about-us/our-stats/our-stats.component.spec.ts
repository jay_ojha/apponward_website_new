import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurStatsComponent } from './our-stats.component';

describe('OurStatsComponent', () => {
  let component: OurStatsComponent;
  let fixture: ComponentFixture<OurStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
