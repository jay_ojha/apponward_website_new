import {
  Component,
  OnInit,
  AfterViewInit,
  Injectable,
  Inject,
  HostListener,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-happyhiv',
  templateUrl: './happyhiv.component.html',
  styleUrls: ['./happyhiv.component.css']
})
export class HappyhivComponent implements OnInit {
  title = 'Happy HIV | Best Dating App | Apponward ';
  constructor(private titleService: Title,
    @Inject(PLATFORM_ID)
    private platformId: any,
    private metaService: Meta,) {

     }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {name: 'description', content: 'Happy HIV is the best dating app for HIV+ people. It is one of the best dating apps in the world for meeting HIV+ folks. It is the best dating app on Play Store.'},
      {name: 'og:image', content: 'https://nzjhgtms.apponward.com/assets/images/apponward.gif'},
      {name: 'keywords', content: '"Happy HIV Portfolio, Happy HIV , Best Dating App, Best Android App for Dating , best dating apps for relationships, best dating apps in the world, the best dating apps, best dating apps for relationships free, best dating apps on iphone, best dating apps worldwide, best dating apps on play store, best app for hiv dating,how to date someone with hiv,  Apponward portfolio, Top App Development Company, Apponward Technologies"'},
      {name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.'},
      {name: 'author', content: 'Apponward Technologies Pvt. Ltd.'},
      {name: 'robots', content: 'index, follow'},
      { name: 'twitter:card', content: 'Happy HIV is the best dating app for HIV+ people. It is one of the best dating apps in the world for meeting HIV+ folks. It is the best dating app on Play Store.'},
      { name: 'twitter:site', content:''},
      { name: 'twitter:creator', content:''}
    ]);
  }
}
