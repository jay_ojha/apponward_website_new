import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServicesComponent } from './services/services.component';
import { AndroidApplicationServiceComponent} from './android-application-service/android-application-service.component';
import { IosApplicationServiceComponent} from './ios-application-service/ios-application-service.component';
import { AwsServiceComponent} from './aws-service/aws-service.component';
import { DigitalMarketingServiceComponent} from './digital-marketing-service/digital-marketing-service.component';
import { UiUxServiceComponent } from './ui-ux-service/ui-ux-service.component';
import { WebApplicationServiceComponent} from './web-application-service/web-application-service.component';
import { HybridMobileAppServiceComponent} from './hybrid-mobile-app-service/hybrid-mobile-app-service.component';
import { SocialMediaMarketingComponent } from './social-media-marketing/social-media-marketing.component';
import { SearchEngineOptimizationComponent } from './search-engine-optimization/search-engine-optimization.component';
import { SearchEngineMarketingComponent } from './search-engine-marketing/search-engine-marketing.component';
import { ContentWritingComponent } from './content-writing/content-writing.component';
import { MobileAppDevelopmentComponent } from './mobile-app-development/mobile-app-development.component';
import { MobileAppsWithFlutterComponent } from './mobile-apps-with-flutter/mobile-apps-with-flutter.component';
import { MobileAppDevelopmentWithReactComponent } from './mobile-app-development-with-react/mobile-app-development-with-react.component';
import { LogoDesignComponent } from './logo-design/logo-design.component';
import { UiUxForWebAppsComponent } from './ui-ux-for-web-apps/ui-ux-for-web-apps.component';
import { UiUxForMobileAppsComponent } from './ui-ux-for-mobile-apps/ui-ux-for-mobile-apps.component';
import { PrototypingComponent } from './prototyping/prototyping.component';
import { WebAppDevelopmentWithPhpLaravelComponent } from './web-app-development-with-php-laravel/web-app-development-with-php-laravel.component';
import { WebAppDevelopmentWithPythonDjangoComponent } from './web-app-development-with-python-django/web-app-development-with-python-django.component';
import { WebAppDevelopmentWithAngularAndNodeComponent } from './web-app-development-with-angular-and-node/web-app-development-with-angular-and-node.component';
import { PwaWithAngularReactComponent } from './pwa-with-angular-react/pwa-with-angular-react.component';
import { DigitalOceanComponent } from './digital-ocean/digital-ocean.component';
import { AzureCloudServicesComponent } from './azure-cloud-services/azure-cloud-services.component';
import { GoogleCloudComponent } from './google-cloud/google-cloud.component';
import { AmazonWebServicesComponent } from './amazon-web-services/amazon-web-services.component';
const routes: Routes = [
  { path: 'services', component: ServicesComponent },
  { path: 'services/android-application-services', component: AndroidApplicationServiceComponent },
  { path: 'services/ios-application-services', component: IosApplicationServiceComponent},
  { path: 'services/cloud-services', component: AwsServiceComponent},
  { path: 'services/digital-marketing-services', component: DigitalMarketingServiceComponent},
  { path: 'services/ui-ux-services', component: UiUxServiceComponent },
  { path: 'services/web-application-services', component: WebApplicationServiceComponent},
  { path: 'services/hybrid-mobile-app-services', component: HybridMobileAppServiceComponent},
  { path: 'services/social-media-marketing', component: SocialMediaMarketingComponent},
  { path: 'services/search-engine-optimization', component: SearchEngineOptimizationComponent},
  { path: 'services/search-engine-marketing', component: SearchEngineMarketingComponent},
  { path: 'services/content-writing', component: ContentWritingComponent},
  { path: 'services/mobile-app-development', component: MobileAppDevelopmentComponent},
  { path: 'services/mobile-apps-with-flutter', component: MobileAppsWithFlutterComponent},
  { path: 'services/mobile-app-development-with-react', component: MobileAppDevelopmentWithReactComponent},
  {path: 'services/logo-design', component:LogoDesignComponent},
  {path: 'services/ui-ux-for-web-apps', component:UiUxForWebAppsComponent},
  {path: 'services/ui-ux-for-mobile-apps', component:UiUxForMobileAppsComponent},
  {path: 'services/prototyping', component:PrototypingComponent},
  {path: 'services/web-app-development-with-php-laravel', component:WebAppDevelopmentWithPhpLaravelComponent},
  {path: 'services/web-app-development-with-python-django', component:WebAppDevelopmentWithPythonDjangoComponent},
  {path: 'services/web-app-development-with-angular-and-node', component:WebAppDevelopmentWithAngularAndNodeComponent},
  {path: 'services/pwa-with-angular-react', component:PwaWithAngularReactComponent},
  {path: 'services/digital-ocean', component:DigitalOceanComponent},
  {path: 'services/azure-cloud-services', component:AzureCloudServicesComponent},
  {path: 'services/google-cloud', component:GoogleCloudComponent},
  {path: 'services/amazon-web-services', component:AmazonWebServicesComponent},
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }
