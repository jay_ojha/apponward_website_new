import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {OurCultureComponent} from './our-culture/our-culture.component'
const routes: Routes = [
  { path:'gallery', component:OurCultureComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GalleryRoutingModule { }
