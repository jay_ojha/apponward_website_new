import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { BlogRoutingModule } from './blog-routing.module';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailsComponent } from './blog-details/blog-details.component';
import { Pipe, PipeTransform } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularButtonLoaderModule } from 'angular-button-loader';
import { HttpClientModule } from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';
import { NgxSocialShareModule } from 'ngx-social-share';
import { BlogDescriptionComponent } from './blog-description/blog-description.component';

@Pipe({
  name: 'truncate'
})

export class TruncatePipe implements PipeTransform {
  transform(value: string, args: any[]): string {
      const limit = args.length > 0 ? parseInt(args[0], 10) : 20;
      const trail = args.length > 1 ? args[1] : '...';
      return value.length > limit ? value.substring(0, limit) + trail : value;
     }
  }


@NgModule({
  declarations: [BlogsComponent, BlogDetailsComponent,TruncatePipe, BlogDescriptionComponent],
  imports: [
    CommonModule,
    BlogRoutingModule,
    SharedModule,
    ToastrModule.forRoot({timeOut: 5000}),
    ReactiveFormsModule,
    FormsModule,
    AngularButtonLoaderModule.forRoot(),
    HttpClientModule,
    NgxPaginationModule,
    NgxSocialShareModule

  ]
})
export class BlogModule { }
