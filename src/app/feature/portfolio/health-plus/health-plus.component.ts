import {
  Component,
  OnInit,
  AfterViewInit,
  Injectable,
  Inject,
  HostListener,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-health-plus',
  templateUrl: './health-plus.component.html',
  styleUrls: ['./health-plus.component.css']
})
export class HealthPlusComponent implements OnInit {
title = 'Health+ | Healthifying yourself | Our Portfolio - Apponward ';
constructor(private titleService: Title,
  @Inject(PLATFORM_ID)
    private platformId: any,
  private metaService: Meta,) {

   }

ngOnInit(): void {
  this.titleService.setTitle(this.title);
  this.metaService.removeTag("name='description'");
  this.metaService.removeTag("name='og:image'");
  this.metaService.removeTag("name='keywords'");
  this.metaService.removeTag("name='ogtag'");
  this.metaService.removeTag("name='author'");
  this.metaService.removeTag("name='robots'");
  this.metaService.removeTag("name='twitter:site'");
  this.metaService.removeTag("name='twitter:creator'");
  this.metaService.addTags([
    {name: 'description', content: 'Health Plus is the perfect partner and the best health app. It is an android and iOS app that tracks the workout, sleep hours, and much more.'},
    {name: 'og:image', content: 'https://nzjhgtms.apponward.com/assets/images/apponward.gif'},
    {name: 'keywords', content: 'Health Plus portfolio, Best Health App, iOS App, Apponward portfolio, Top App Development Company, Apponward Technologies'},
    {name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.'},
    {name: 'author', content: 'Apponward Technologies Pvt. Ltd.'},
    {name: 'robots', content: 'index, follow'},
    { name: 'twitter:card', content: 'Health Plus is the perfect partner and the best health app. It is an android and iOS app that tracks the workout, sleep hours, and much more.'},
    { name: 'twitter:site', content:''},
    { name: 'twitter:creator', content:''}
  ]);
}
@HostListener('window:popstate', ['$event'])
  onPopState() {
    console.log('Back button pressed');
    if (isPlatformBrowser(this.platformId)) {
    sessionStorage.setItem('back_btn', 'true');
    }
  }
}
