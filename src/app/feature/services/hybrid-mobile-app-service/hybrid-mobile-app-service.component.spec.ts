import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HybridMobileAppServiceComponent } from './hybrid-mobile-app-service.component';

describe('HybridMobileAppServiceComponent', () => {
  let component: HybridMobileAppServiceComponent;
  let fixture: ComponentFixture<HybridMobileAppServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HybridMobileAppServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HybridMobileAppServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
