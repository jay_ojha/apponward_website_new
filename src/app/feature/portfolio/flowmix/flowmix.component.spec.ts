import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowmixComponent } from './flowmix.component';

describe('FlowmixComponent', () => {
  let component: FlowmixComponent;
  let fixture: ComponentFixture<FlowmixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlowmixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowmixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
