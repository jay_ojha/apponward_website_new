import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-about-work',
  templateUrl: './about-work.component.html',
  styleUrls: ['./about-work.component.css'],
})
export class AboutWorkComponent implements OnInit {
  constructor(
    @Inject(PLATFORM_ID)
    private platformId: any
  ) {}

  ngOnInit(): void {}
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    // debugger;
    if (isPlatformBrowser(this.platformId)) {
      console.log('Back button pressed');
      sessionStorage.setItem('back_btn', 'true');
    }
  }
}
