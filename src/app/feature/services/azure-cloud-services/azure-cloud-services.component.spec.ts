import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzureCloudServicesComponent } from './azure-cloud-services.component';

describe('AzureCloudServicesComponent', () => {
  let component: AzureCloudServicesComponent;
  let fixture: ComponentFixture<AzureCloudServicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzureCloudServicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzureCloudServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
