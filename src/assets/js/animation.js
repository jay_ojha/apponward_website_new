/*const typedTextSpan = document.querySelector(".typed-text");
const cursorSpan = document.querySelector(".cursor");
const textArray =  [
"Mobile App Development" ,
"iPhone App Development" ,
"Android App Development" ,
"iPad App Development" ,
"iOS App Development" ,
"Mobile Apps with Flutter" ,
"Mobile Apps with React Native" ,
"Web App Development" ,
"Web Apps with PHP/Laravel" ,
"Web Apps with Python/Django" ,
"Web Apps with Angular and Node" ,
"PWA with Angular/React" ,
"UI/UX Designs" ,
"UI/UX for Mobile App" ,
"Ui/UX for web App" ,
"Logo Design" ,
"Prototyping" ,
"Cloud Services" ,
"Amazon Web Services" ,
"Google Cloud" ,
"Azure Cloud Services" ,
"Digital Ocean" ,
"DIgital Marketing" ,
"Social Media Marketing (SMM)" ,
"Search Engine Optimization (SEO)" ,
"Google Ads / PPC (Pay-per-click)" ,
"Content Writing" 
];
const typingDelay = 200;
const erasingDelay = 100;
const newTextDelay = 2000; // Delay between current and next text
let textArrayIndex = 0;
let charIndex = 0;

function type() {
    if (charIndex < textArray[textArrayIndex].length) {
        if (!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
        typedTextSpan.textContent += textArray[textArrayIndex].charAt(charIndex);
        charIndex++;
        setTimeout(type, typingDelay);
    }
    else {
        cursorSpan.classList.remove("typing");
        setTimeout(erase, newTextDelay);
    }
}

function erase() {
    if (charIndex > 0) {
        if (!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
        typedTextSpan.textContent = textArray[textArrayIndex].substring(0, charIndex - 1);
        charIndex--;
        setTimeout(erase, erasingDelay);
    }
    else {
        cursorSpan.classList.remove("typing");
        textArrayIndex++;
        if (textArrayIndex >= textArray.length) textArrayIndex = 0;
        setTimeout(type, typingDelay + 1100);
    }
}
document.addEventListener("DOMContentLoaded", function () { // On DOM Load initiate the effect
    if (textArray.length) setTimeout(type, newTextDelay + 250);
});*/