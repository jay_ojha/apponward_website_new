import {
  Component,
  OnInit,
  HostListener,
  AfterViewInit,
  Injectable,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-about-banner',
  templateUrl: './about-banner.component.html',
  styleUrls: ['./about-banner.component.css'],
})
export class AboutBannerComponent implements OnInit {
  constructor(
    @Inject(PLATFORM_ID)
    private platformId: any
  ) {}

  ngOnInit(): void {}
  @HostListener('window:popstate', ['$event'])
  onPopState() {
    if (isPlatformBrowser(this.platformId)) {
      console.log('Back button pressed');
      sessionStorage.setItem('back_btn', 'true');
    }
  }
}
