import { Component, OnInit } from '@angular/core';

import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-web-app-development-with-angular-and-node',
  templateUrl: './web-app-development-with-angular-and-node.component.html',
  styleUrls: ['./web-app-development-with-angular-and-node.component.css']
})
export class WebAppDevelopmentWithAngularAndNodeComponent implements OnInit {
  title = 'Best Web Apps with Angular and Node - Apponward';

  constructor(
    private titleService: Title,
    private metaService: Meta
  ) { }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.removeTag("name='description'");
    this.metaService.removeTag("name='og:image'");
    this.metaService.removeTag("name='keywords'");
    this.metaService.removeTag("name='ogtag'");
    this.metaService.removeTag("name='author'");
    this.metaService.removeTag("name='robots'");
    this.metaService.removeTag("name='twitter:site'");
    this.metaService.removeTag("name='twitter:creator'");
    this.metaService.addTags([
      {
        name: 'description',
        content:
          'Apponward is a reputable Web App Development company. Get the best web development using Angular & Node. We have expertise in Angular website development.',
      },
      {
        name: 'og:image',
        content:
          'https://lh5.googleusercontent.com/p/AF1QipNMS4ebHVvp8eAXp1GzERiq-KsPMwiIp1CUnlq8=w203-h116-k-no',
      },
      {
        name: 'keywords',
        content:
          'web development with Angular,  web development using Node, how to use Angular & Node in web development, Angular website development,  web development using Angular & Node, web development through AngularButtonLoaderDirective, web development by Angular & Node,   web page development with Node, front end web development with Angular',
      },
      { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
      { name: 'robots', content: 'index, follow' },
      {
        name: 'twitter:card',
        content:
          'Apponward is a reputable Web App Development company. Get the best web development using Angular & Node. We have expertise in Angular website development.',
      },
      { name: 'twitter:site', content: '' },
      { name: 'twitter:creator', content: '' },
    ]);
  }

}
