import { Component, OnInit } from '@angular/core';

import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-web-app-development-with-python-django',
  templateUrl: './web-app-development-with-python-django.component.html',
  styleUrls: ['./web-app-development-with-python-django.component.css']
})
export class WebAppDevelopmentWithPythonDjangoComponent implements OnInit {
  title = 'Best Web App Development with Python/Django - Apponward';


  constructor(private titleService: Title,
    private metaService: Meta) { }

    ngOnInit(): void {
      this.titleService.setTitle(this.title);
      this.metaService.removeTag("name='description'");
      this.metaService.removeTag("name='og:image'");
      this.metaService.removeTag("name='keywords'");
      this.metaService.removeTag("name='ogtag'");
      this.metaService.removeTag("name='author'");
      this.metaService.removeTag("name='robots'");
      this.metaService.removeTag("name='twitter:site'");
      this.metaService.removeTag("name='twitter:creator'");
      this.metaService.addTags([
        {
          name: 'description',
          content:
            'Apponward is a leading python website development company. We offer the best web development web development using python and django',
        },
        {
          name: 'og:image',
          content:
            'https://lh5.googleusercontent.com/p/AF1QipNMS4ebHVvp8eAXp1GzERiq-KsPMwiIp1CUnlq8=w203-h116-k-no',
        },
        {
          name: 'keywords',
          content:
            'web development with python web, development in python, web development using python,  how to use python in web development , python website development, web development using python and django, python web development with django, projects python web development tools,  web development through python, web development by python,  web page development with python,  front end web development with python , web development with python django',
        },
        { name: 'ogtag', content: 'Apponward Technologies Pvt. Ltd.' },
        { name: 'author', content: 'Apponward Technologies Pvt. Ltd.' },
        { name: 'robots', content: 'index, follow' },
        {
          name: 'twitter:card',
          content:
            'Apponward is a leading python website development company. We offer the best web development web development using python and django',
        },
        { name: 'twitter:site', content: '' },
        { name: 'twitter:creator', content: '' },
      ]);
    }

}
