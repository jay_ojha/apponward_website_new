import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebAppDevelopmentWithPhpLaravelComponent } from './web-app-development-with-php-laravel.component';

describe('WebAppDevelopmentWithPhpLaravelComponent', () => {
  let component: WebAppDevelopmentWithPhpLaravelComponent;
  let fixture: ComponentFixture<WebAppDevelopmentWithPhpLaravelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebAppDevelopmentWithPhpLaravelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebAppDevelopmentWithPhpLaravelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
